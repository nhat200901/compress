/** @type {import('next').NextConfig} */
const isProd = process.env.NODE_ENV === 'production';
const SUB_DIRECTORY = '';

const nextConfig = {
  env: {
    API_HOST: process.env.API_HOST,
    basePath: isProd ? SUB_DIRECTORY : '',
    assetPrefix: isProd ? SUB_DIRECTORY : '',
    publicRuntimeConfig: {
      basePath: isProd ? SUB_DIRECTORY : '',
    },
    apiPath: 'http://localhost:8000',
  }
}

module.exports = nextConfig
