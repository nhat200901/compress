"use client";
import type { Metadata } from "next";
// import './globals.css'
import { Inter } from "next/font/google";
import "bootstrap/dist/css/bootstrap.min.css";
import { SWRConfig } from "swr";
import { Toaster } from "react-hot-toast";

const inter = Inter({ subsets: ["latin"] });


export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <SWRConfig
          value={{
            revalidateOnFocus: false,
          }}
        >
          {children}
          <Toaster position="bottom-right" />
        </SWRConfig>
      </body>
    </html>
  );
}
