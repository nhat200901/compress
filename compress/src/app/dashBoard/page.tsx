"use client";
import styles from "@/styles/page.module.css";
import useSWR from "swr";
import DataTable from "@/components/share/control/Table/Table";
import { Column } from "react-table";
import { useMemo } from "react";
import { Transaction } from "@/model/dashboard/transaction";
import { getTransactions, transactionApiUrl } from "@/services/dashboard";
import Layout from "@/components/share/layout";

export default function Dashboard() {
  debugger;
  const {
    isLoading: isLoadingTransation,
    data: transactions,
    mutate,
  } = useSWR(transactionApiUrl, getTransactions);

  const columns: Column<Transaction>[] = useMemo(
    () => [
      { Header: "OrderId", accessor: "orderId" },
      { Header: "Account Type", accessor: "accountType" },
      { Header: "Type", accessor: "type" },
      { Header: "Action Exchange", accessor: "actionExchange" },
      { Header: "Amount", accessor: "amount" },
      {
        Header: "Order Date",
        accessor: "orderDate",
      },
      { Header: "Remarks", accessor: "remarks" },
    ],
    []
  );

  return (
    <Layout>
      <main className={styles.main}>
        {isLoadingTransation ? (
          "Loading..."
        ) : (
          <DataTable<Transaction> columns={columns} data={transactions} />
        )}
      </main>
    </Layout>
  );

}
