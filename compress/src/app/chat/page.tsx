"use client";
import React, { useEffect, useRef, useState } from "react";

import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";

import { Button, Alert, Form } from 'react-bootstrap';
import Layout from "@/components/share/layout";

import { DataContext } from "@/model/chat/DataContext";
import setting from "@/components/share/setting";
import SharedData from '@/model/chat/SharedData';
import Cookies from "js-cookie";
import { cookieKey } from "@/constants/authCommon";
import { User } from "@/model/user";
import { MessageViewModel } from "@/model/chat/MessageViewModel";
import { ChatParticipantViewModel } from "@/model/chat/ChatParticipantViewModel";
import { chatMessage, getConversation } from "@/services/chat/chatService";
import { useNotification } from '@/lib/hooks/useNotification';
import { ChatRoom } from "@/components/chat/chat-room";
import { useIntersection } from '@/lib/hooks/useIntersection';
import { AnimatePresence } from 'framer-motion';
import { ImageModal } from '@/components/modal/image-modal';
import type { MessageData, ImageData } from '@/components/form/main-form-chat';
import { MainForm } from '@/components/form/main-form-chat';
//import "@/styles/chat.css";
//import "@/styles/boxchat.css";
import "@/styles/dialogchat.css";
import { ContainerChat } from "@/components/chat/container-chat";
import { HeaderChat } from "@/components/chat/header-chat";
import { DialogChat } from "@/components/modal/dialog-chat";

export default function ChatPage() {

    const [connection, setConnection] = useState<HubConnection | null>(null);
    const [messages, setMessages] = useState<string[]>([]);
    const [ready, setReady] = useState<boolean>(false);
    const [error, setError] = useState<string | null>(null);
    const [currentUser, setCurrentUser] = useState<User | null>(null);
    const [messageSend, setMessageSend] = useState<MessageData | null>(null);

    //#region  
    // const [user, loading, error] = useAuthState(auth);
    // const CurrentUser = null;
    const currentUserId = null;
    const [isEditMode, setIsEditMode] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [messageData, setMessageData] = useState<MessageData | null>(null);
    const [imageData, setImageData] = useState<ImageData | null>(null);

    const scrollArea = useRef<HTMLOListElement>(null);
    const bottomSpan = useRef<HTMLSpanElement>(null);

    const isNotificationAllowed = useNotification();
    const [showDialog, setShowDialog] = useState(false);
    // const isAtBottom = useIntersection(scrollArea, bottomSpan, {
    //     rootMargin: '0px 0px 300px',
    //     threshold: 1.0
    // });
    const messagesLength = 5;
    // const [messagesSnapshot, messagesLength] = await Promise.all([
    //     getDocs(getMessagesQuery(20)),
    //     getMessagesSize()
    //   ]);
    const openModal = (data: ImageData) => (): void => {
        setIsModalOpen(true);
        setImageData(data);
    };
    const goToEditMode = (docId: string, docText: string) => (): void => {
        //setMessageData({ docId, docText });
        setIsEditMode(true);
    };
    const exitEditMode = (): void => {
        setIsEditMode(false);
        setTimeout(() => setMessageData(null), 500);
    };

    const scrollToBottom = (input?: boolean, delay?: number): void => {
        if (messageData || (input)) return;//&& isAtBottom
        setTimeout(
            () => bottomSpan.current?.scrollIntoView({ behavior: 'smooth' }),
            delay ?? 100
        );
    };
    const closeModal = (): void => setIsModalOpen(false);
    // end 
    const handleSendMessage = async (messagetext: string) => {
        debugger;
        setError(null);
        const user = currentUser?.username;
        if (currentUser) {
            const messageObj = {
                senderUserName: currentUser.username,
                senderUserId: currentUser.userId,
                recieverUserId: "2025edf4-c748-42dd-89c6-1f0cb64edbb9",
                recieverUserName: "chat1",
                message: messagetext,
                conversationId: "515c37c8-646e-4185-8844-0c2b1f97c931"
            };
            //setMessageSend(message);
            connection!.invoke("SendMessage", messageObj).then(async () => {
                debugger;
                setMessages([`${messageObj.senderUserName}: ${messageObj.message}`, ...messages]);
                await chatMessage(messageObj);
                await getConversation("515c37c8-646e-4185-8844-0c2b1f97c931");
                // save to server
            }
            ).catch((err: Error) => {
                setError(`${err}`);
            });
        }

    };
    const joinChat = () => {
        if (connection != null && connection.connectionId != null) {
            connection!.invoke("Join", currentUser?.username, currentUser?.userId).catch((err: Error) => {
                setError(`${err}`);
            });
        }
    };
    useEffect(() => {
        const userInfo = Cookies.get(cookieKey);
        if (userInfo) {
            setCurrentUser(JSON.parse(userInfo));
        }
        const newConnection = new HubConnectionBuilder()
            .withUrl(`${setting.apiPath}/chat`)
            .build();
        setConnection(newConnection);

    }, []);

    useEffect(() => {
        if (connection) {
            connection
                .start()
                .then(() => {
                    setReady(true);
                    joinChat();
                })
                .catch((err: Error) => {
                    setError(`${err}`);
                });
        }
    }, [connection]);

    useEffect(() => {
        if (connection) {
            connection.on("messageReceived", (participant: ChatParticipantViewModel, message: MessageViewModel) => {
                setMessages([`${participant.displayName}: ${message.message}`, ...messages]);
            });
        }
    }, [connection, messages]);

    const { sharedData, setSharedData } = React.useContext(DataContext);
    //#region  for popup
    async function onClose() {
        setShowDialog(false)
        console.log("Modal has closed")
    }

    async function onOk() {
        setShowDialog(false)
        console.log("Ok was clicked")
    }
    function moForm() {
        debugger;
        setShowDialog(true);

        console.log("Ok was clicked")
    }
    //#endregion

    return (
        <Layout>
            <button id="chat" className="button-chatbox" onClick={() => moForm()}>Chat</button>
            {/* <button onClick={() => { setShowDialog(true) }}>open modal</button> */}

            <DialogChat title="Live Chat" showDialog={showDialog} onClose={onClose} onOk={onOk} handleSendMessage={handleSendMessage}>
                {/* <ChatRoom
                    scrollArea={scrollArea}
                    bottomSpan={bottomSpan}
                    //isAtBottom={isAtBottom}
                    //messagesProp={messagesProp}
                    currentUserId={currentUserId}
                    messagesLength={messagesLength}
                    openModal={openModal}
                    goToEditMode={goToEditMode}
                    exitEditMode={exitEditMode}
                    scrollToBottom={scrollToBottom}
                />
                <AnimatePresence>
                    {isModalOpen && imageData && (
                        <ImageModal imageData={imageData} closeModal={closeModal} />
                    )}
                </AnimatePresence>
                <MainForm
                    isEditMode={isEditMode}
                    messageData={messageData}
                    currentUserId={currentUserId}
                    openModal={openModal}
                    exitEditMode={exitEditMode}
                    scrollToBottom={scrollToBottom}
                /> */}
            </DialogChat>

            {/* <div className="friends">
                <ul id='friend-list'>
                    <li className='friend selected'>
                        <img src='https://i.imgur.com/nkN3Mv0.jpg' />
                        <div className='name'>
                            Andres Perez
                        </div>
                    </li>
                </ul>
            </div> */}

            {/* <!-- Main container   --> */}
            {/* <ContainerChat>
                <HeaderChat
                    //error={error}
                    //loading={loading}
                    userInfo={currentUser}
                    isNotificationAllowed={isNotificationAllowed}
                />
            </ContainerChat> */}
            {/* <ChatRoom
                scrollArea={scrollArea}
                bottomSpan={bottomSpan}
                isAtBottom={isAtBottom}
                //messagesProp={messagesProp}
                currentUserId={currentUserId}
                messagesLength={messagesLength}
                openModal={openModal}
                goToEditMode={goToEditMode}
                exitEditMode={exitEditMode}
                scrollToBottom={scrollToBottom}
            />
            <AnimatePresence>
                {isModalOpen && imageData && (
                    <ImageModal imageData={imageData} closeModal={closeModal} />
                )}
            </AnimatePresence>
            <MainForm
                isEditMode={isEditMode}
                messageData={messageData}
                currentUserId={currentUserId}
                openModal={openModal}
                exitEditMode={exitEditMode}
                scrollToBottom={scrollToBottom}
            /> */}
        </Layout >
    );
};
