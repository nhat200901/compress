"use client";
import { useRouter } from "next/navigation";
import { FormEvent, useState } from "react";
import { useAuth } from "@/lib/useAuth";
import { Col, Button, Row, Container, Card, Form } from "react-bootstrap";
import toast from "react-hot-toast";
import { redirectAfterLoginPage } from "@/constants/authCommon";

export default function Login() {
  const { login } = useAuth();
  const router = useRouter();
  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const email = e.currentTarget.inputEmail.value;
    const pwd = e.currentTarget.inputPassword.value;

    if (!email || !pwd) {
      toast.error("Please enter information");
    } else {
      login(email, pwd)
        .then((res) => router.push(redirectAfterLoginPage))
        .catch((e) => {
          toast.error(e?.message);
        });
    }
  };

  return (
    <div className="login-page">
      <Container>
        <Row className="vh-100 d-flex justify-content-center align-items-center">
          <Col md={8} lg={6} xs={12}>
            <div className="text-center mb-5">
              <h2 className="fw-bold mb-2 text-uppercase ">ValenPay</h2>
            </div>

            <Card className="shadow">
              <Card.Body>
                <div className="mb-3 mt-md-4">
                  <div className="mb-3">
                    <Form onSubmit={onSubmit}>
                      <Form.Group className="mb-3" controlId="inputEmail">
                        <Form.Label className="text-center">
                          Email address
                        </Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                      </Form.Group>

                      <Form.Group className="mb-3" controlId="inputPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                      </Form.Group>

                      <div className="d-grid">
                        <Button variant="primary" type="submit">
                          Login
                        </Button>
                      </div>
                      <p className="small text-center mt-3">
                        <a className="text-primary" href="#!">
                          Forgot password?
                        </a>
                      </p>
                    </Form>
                  </div>
                </div>
              </Card.Body>
            </Card>
            <div className="text-center mt-5">Copyright ValenPay</div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
