"use client";
import { useState } from "react";
import Menu from "@/components/share/control/Menu/Menu";
import { Button, Container, Nav } from "react-bootstrap";
import styles from "@/styles/styles.module.css";

export default function Layout({
  children,
}: {
  children: React.ReactNode;
}) {
  const [isOpenSidebar, setIsOpenSidebar] = useState(true);

  const toggleSidebar = () => {
    setIsOpenSidebar((isOpen) => !isOpen);
  };

  return (
    <div className="d-flex">
      <div className={`"${styles.sidebar}" ${isOpenSidebar ? "is-open" : ""}`}>
        <Menu />
      </div>

      {/* <Container fluid className={styles.content ${isOpenSidebar ? "is-open" : ""}`}> */}
      <Container fluid className={styles.content}>
        {children}
      </Container>
    </div>
  );
}

// import React, { ReactNode } from 'react';
// import Head from 'next/head';
// import setting from '../setting';

// type Props = {
//   children?: ReactNode,
//   title?: string,
//   menu?: boolean,
//   footer?: boolean,
// };

// const Layout = ({ children, title = setting.title, menu = true, footer = true }: Props) => (
//   <div>
//     <Head>
//       <title>{title}</title>
//       <meta charSet="utf-8" />
//       <meta name="viewport" content="initial-scale=1.0, width=device-width" />
//       <link rel="shortcut icon" href={`${setting.basePath}favicon.ico`} type="image/x-icon" />
//     </Head>
//     <div id="Wrapper">
//       {menu ? <><main>{children}</main><Menu /></> : children}
//     </div>
//     {footer && <footer><a target='_blank' rel="noreferrer">Valenspay</a></footer>}
//   </div>
// );

