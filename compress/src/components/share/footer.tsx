export default function Footer() {
    return (
      <h1 className="font-bold text-5xl text-transparent bg-clip-text bg-gradient-to-r from-purple-500 via-pink-500 to-pink-500">
        Valenspay footer nextjs
      </h1>
    );
  }