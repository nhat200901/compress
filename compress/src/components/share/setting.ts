import Env from '@/../next.config';
const isProd = process.env.NODE_ENV === 'production';

const setting = {
    isProd,
    basePath: Env.env.basePath,
    apiPath: 'http://localhost:5702',
    title: 'Customer Service',
};

export default setting;
