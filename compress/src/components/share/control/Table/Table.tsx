"use client";
import { Col, Form, Pagination, Row } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import { Column, useGlobalFilter, usePagination, useTable } from "react-table";

interface ReactTableProps<T extends object> {
  data: T[];
  columns: Column<T>[];
  initialState?: Record<string, unknown>;
}

const DataTable = <T extends object>(props: ReactTableProps<T>) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    // rows,
    state,
    prepareRow,
    setGlobalFilter,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
  } = useTable(
    {
      columns: props.columns,
      data: props.data,
    },
    useGlobalFilter,
    usePagination
  );
  const { globalFilter, pageIndex, pageSize } = state;

  if (!props.data?.length) {
    return <div>Nodata</div>;
  }

  return (
    <div className="data-table">
      <div className="search-container mb-3">
        <Row>
          <Col lg={4} sm={6}>
            <Form.Control
              placeholder="Search..."
              type="text"
              value={globalFilter || ""}
              onChange={(e) => setGlobalFilter(e.target.value)}
            />
          </Col>
        </Row>
      </div>
      <Table striped bordered hover {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup: any) => {
            const { key, ...restHeaderProps } =
              headerGroup.getHeaderGroupProps();
            return (
              <tr key={key} {...restHeaderProps}>
                {headerGroup.headers.map((column: any) => {
                  const { key, ...restColumnProps } = column.getHeaderProps({
                    style: { minWidth: column.minWidth, width: column.width },
                  });
                  return (
                    <th key={key} {...restColumnProps}>
                      {column.render("Header")}
                    </th>
                  );
                })}
              </tr>
            );
          })}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page?.length
            ? page.map((row: any) => {
                prepareRow(row);
                return (
                  <tr key={row.id} {...row.getRowProps}>
                    {row.cells.map((cell: any) => {
                      const { key, ...restCellProps } = cell.getCellProps({
                        style: {
                          minWidth: cell.column.minWidth,
                          width: cell.column.width,
                        },
                      });
                      return (
                        <td key={key} {...restCellProps}>
                          {cell.render("Cell")}
                        </td>
                      );
                    })}
                  </tr>
                );
              })
            : "No data"}
        </tbody>
      </Table>

      <Row>
        <Col sm={4}>
          <Pagination className="pagination">
            <Pagination.First
              onClick={() => gotoPage(0)}
              disabled={!canPreviousPage}
            />
            <Pagination.Prev
              onClick={() => previousPage()}
              disabled={!canPreviousPage}
            />

            <Pagination.Next
              onClick={() => nextPage()}
              disabled={!canNextPage}
            />
            <Pagination.Last
              onClick={() => gotoPage(pageCount - 1)}
              disabled={!canNextPage}
            />
          </Pagination>
        </Col>
        <Col sm={5}>
          <div>
            Page{" "}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{" "}
          </div>
        </Col>
        <Col sm={3}>
          <Form.Select
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </Form.Select>
        </Col>
      </Row>
    </div>
  );
};
export default DataTable;
