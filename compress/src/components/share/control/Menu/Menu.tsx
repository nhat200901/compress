"use client";
import { MENU } from "@/constants/common";
import { Nav } from "react-bootstrap";
import styles from "@/styles/styles.module.css";

export default function Menu() {
  return (
    <div >
      <Nav className="flex-column p-2">
        <h3 className="text-center">ValenPay</h3>
        <hr />
        {MENU.map((menu) => (
          <Nav.Item key={menu.url}>
            <Nav.Link href={menu.url}>{menu.name}</Nav.Link>
          </Nav.Item>
        ))}
      </Nav>
    </div>
  );
}
