import type { ReactNode } from 'react';
import "@/styles/dialogchat.css";

type ContainerChatProps = {
    children: ReactNode;
};

export function ContainerChat({ children }: ContainerChatProps): JSX.Element {
    return (
        <div className='container-chat'>
            {children}
        </div>
    );
}
