import cn from 'clsx';
import { Button } from '@/components/ui/button';
import { User } from "@/model/user";
import {
    VscLoading,
    RiGithubFill,
    RiGoogleFill,
    RiLogoutBoxLine,
    RiErrorWarningLine,
    RiNotificationFill,
    RiNotificationOffFill
} from '@/../assets/icons';

type HeaderChatProps = {
    //error: Error | undefined;
    //loading: boolean;
    userInfo: User | null;
    isNotificationAllowed: boolean;
};

export function HeaderChat({
    //error,
    //loading,
    userInfo,
    isNotificationAllowed
}: HeaderChatProps): JSX.Element {
    const { Icon, label } = userInfo
        ? {
            Icon: RiLogoutBoxLine,
            label: 'Sign Out'
        }
        : {
            Icon: RiGoogleFill,
            label: 'Sign In'
        };

    return (
        // <header className='flex justify-between font-bold text-primary/80'>
        //     <a
        //         className='smooth-tab smooth-hover custom-button'
        //         target='_blank'
        //         rel='noreferrer'
        //     >
        //         <RiGithubFill className='text-xl' />
        //         <p className='text-lg'>ccrsxx</p>
        //     </a>
        //     {userInfo && (
        //         <Button
        //             className={cn('animate-fade', {
        //                 'text-green-400': isNotificationAllowed,
        //                 'text-red-400': !isNotificationAllowed
        //             })}
        //             disabled={isNotificationAllowed}
        //         >
        //             {isNotificationAllowed ? (
        //                 <RiNotificationFill />
        //             ) : (
        //                 <RiNotificationOffFill />
        //             )}
        //             <span className='hidden md:block'>
        //                 Notification {isNotificationAllowed ? 'on' : 'off'}
        //             </span>
        //         </Button>
        //     )}
        //     {
        //         <i className='flex w-14 items-center'>
        //             <VscLoading className='animate-spin' size={20} />
        //         </i>
        //     }
        // </header>
        <div className="msg-header">
            <div className="container1">
                {/* <img src="user1.png" class="msgimg"> */}
                <div className="active ">
                    <p>{userInfo?.username}</p>
                </div>
            </div>
        </div>
    );
}
