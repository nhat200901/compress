/* eslint-disable react-hooks/exhaustive-deps */

import { useState, useEffect, useRef } from 'react';
import { AnimatePresence } from 'framer-motion';
import { useIntersection } from '@/lib/hooks/useIntersection';

import { Button } from '@/components/ui/button';
import { FaFan } from '@/../assets/icons';
import { ChatSkeleton } from '@/components/ui/chat-skeleton';
import { ChatMessage } from './chat-message';
import type { MutableRefObject } from 'react';
import type { ImageData } from '@/components/form/main-form-chat';

type ChatRoomProps = {
    scrollArea: MutableRefObject<HTMLOListElement | null>;
    bottomSpan: MutableRefObject<HTMLSpanElement | null>;
    //isAtBottom: boolean;
    // messagesProp: Messages;
    currentUserId: string | null;
    messagesLength: number;
    openModal: (data: ImageData) => () => void;
    goToEditMode: (docId: string, text: string) => () => void;
    exitEditMode: () => void;
    scrollToBottom: (input?: boolean) => void;
};

export const ADMIN_ID = process.env.NEXT_PUBLIC_ADMIN_ID;

export function ChatRoom({
    scrollArea,
    bottomSpan,
    //isAtBottom,
    //messagesProp,
    currentUserId,
    messagesLength,
    openModal,
    goToEditMode,
    exitEditMode,
    scrollToBottom
}: ChatRoomProps): JSX.Element {
    //const [messages, setMessages] = useState<Messages>(messagesProp);
    const [messageLimit, setMessageLimit] = useState(20);
    const [messagesSize, setMessagesSize] = useState(messagesLength);
    const [isReachedLimit, setIsReachedLimit] = useState(false);

    // const [messagesDb, loading] = useCollectionData(
    //     getMessagesQuery(messageLimit)
    // );

    const topSkeleton = useRef<HTMLDivElement>(null);

    // const isAtTop = useIntersection(scrollArea, topSkeleton, {
    //     rootMargin: '100px 0px 0px',
    //     threshold: 0.1
    // });

    // const isNearTop = useIntersection(scrollArea, topSkeleton, {
    //     rootMargin: '1300px 0px 0px',
    //     threshold: 0.1
    // });

    useEffect(() => {
        const checkLimit =
            messagesSize !== null ? messageLimit >= messagesSize : false;
        setIsReachedLimit(checkLimit);
    }, [messagesSize, messageLimit]);

    // useEffect(() => {
    //     if (isReachedLimit) return;

    //     let timeoutId: NodeJS.Timeout;

    //     if ((isAtTop || isNearTop) && !loading) {
    //         timeoutId = setTimeout(() => setMessageLimit(messageLimit + 20), 1000);
    //         // const firstMessageId = messages[0]?.id;

    //         // if (firstMessageId) {
    //         //     scrollToChat(firstMessageId, 500);
    //         //     scrollToChat(firstMessageId, 1000);
    //         // }
    //     }

    //     return () => clearTimeout(timeoutId);
    // }, [isNearTop, loading]);

    // useEffect(() => {
    //     if (!loading) {
    //         if (isAtBottom) scrollToBottom();

    //         //setMessages(messagesDb as Messages);
    //         void setDocumentLength();
    //     }
    // }, [messagesDb]);

    // const scrollToChat = (chatId: string, delay?: number): void => {
    //     const chatElement = document.getElementById(chatId);
    //     setTimeout(() => {
    //         if (!isAtTop) return;
    //         chatElement?.scrollIntoView();
    //     }, delay ?? 100);
    // };

    const setDocumentLength = async (): Promise<void> => {
        //const messagesSize = await getMessagesSize();
        // setMessagesSize(messagesSize);
    };

    const isAdmin = currentUserId === ADMIN_ID;

    return (
        <div id="chat" className="chat_box_wrapper chat_box_small chat_box_active" >
            <div className="chat_box touchscroll chat_box_colors_a">
                <div className="chat_message_wrapper">
                    <div className="chat_user_avatar">
                        <a href="https://web.facebook.com/iamgurdeeposahan" target="_blank">
                            {/* <ImageLoaderLegacy
                                                src={"https://bootdey.com/img/Content/avatar/avatar1.png"}
                                                alt={"imageData.alt"}
                                                onClick={() => { }}
                                            /> */}

                        </a>
                    </div>
                    <ul className="chat_message">
                        <li>
                            <p>  <span className="chat_message_time">Customer service: 13:34 PM</span> </p>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio, eum? </p>
                        </li>


                    </ul>
                </div>
                <div className="chat_message_wrapper chat_message_right">
                    <div className="chat_user_avatar">
                        {/* <a href="https://web.facebook.com/iamgurdeeposahan" target="_blank">
                                            <img alt="Gurdeep Osahan (Web Designer)" title="Gurdeep Osahan (Web Designer)" src="https://bootdey.com/img/Content/avatar/avatar1.png" className="md-user-image">
                                        </a> */}
                    </div>
                    <ul className="chat_message">
                        <li>
                            <p>
                                <span className="chat_message_time">Customer service: 13:34 PM</span>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem delectus distinctio dolor earum est hic id impedit ipsum minima mollitia natus nulla perspiciatis quae quasi, quis recusandae, saepe, sunt totam.

                            </p>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        // <ol
        //     className='flex flex-1 flex-col-reverse gap-4 overflow-y-auto
        //          overflow-x-hidden rounded-lg bg-neutral-800 px-4 pt-4'
        //     ref={scrollArea}
        // >
        //     {isAdmin && (
        //         <div className='group fixed z-10 -translate-x-4 translate-y-0 rounded-lg p-4'>
        //             <Button
        //                 className='translate-y-4 bg-bubble p-2 text-lg text-primary/80 opacity-0
        //                hover:bg-red-400 group-hover:translate-y-0 group-hover:opacity-100'
        //                 iconStyle='animate-spin'
        //                 Icon={FaFan}
        //                 //onClick={deleteAllMessages}
        //                 tabIndex={-1}
        //             />
        //         </div>
        //     )}
        //     <div className='grid grid-flow-row auto-rows-min gap-3 md:gap-4'>
        //         <ChatSkeleton
        //             topSkeleton={topSkeleton}
        //             visible={!isReachedLimit && !!(messagesSize && messagesSize >= 20)}
        //         />
        //         <AnimatePresence initial={false}>
        //             {/* {messages.map(({ ...rest }) => (
        //                 <ChatMessage
        //                     isAdmin={isAdmin}
        //                     currentUserId={currentUserId}
        //                     openModal={openModal}
        //                     goToEditMode={goToEditMode}
        //                     exitEditMode={exitEditMode}
        //                     {...rest}
        //                     key={rest.id}
        //                 />
        //             ))} */}
        //         </AnimatePresence>
        //         <span ref={bottomSpan} />
        //     </div>
        // </ol>
    );
}
