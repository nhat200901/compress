"use client"

import { useRef, useEffect, useState } from 'react'
import "@/styles/dialogchat.css";
import { ImageLoaderLegacy } from '../ui/image-loader-legacy';
import { AnimatePresence } from 'framer-motion';
import { type MessageData, type ImageData, MainForm } from '@/components/form/main-form-chat';
import { ImageModal } from './image-modal';
import TextareaAutosize from 'react-textarea-autosize';
import cn from 'clsx';
import { Button } from '../ui/button';
import { RiImageAddLine, RiSendPlane2Line } from '@/../assets/icons';
import { ChatRoom } from '../chat/chat-room';

type DialogChatProps = {
    title: string,
    showDialog: boolean;
    onClose: () => void,
    onOk: () => void,
    children: React.ReactNode,
    handleSendMessage: (mesage: string) => void,
}

export function DialogChat({ title, showDialog, onClose, onOk, children, handleSendMessage }: DialogChatProps) {

    const dialogRef = useRef<null | HTMLDialogElement>(null)

    useEffect(() => {
        if (showDialog) {
            dialogRef.current?.showModal()
        } else {
            dialogRef.current?.close()
        }
    }, [showDialog])

    const closeDialog = () => {
        dialogRef.current?.close()
        onClose()
    }

    const clickOk = () => {
        onOk()
        closeDialog()
    }
    //#region  
    // const [user, loading, error] = useAuthState(auth);
    // const CurrentUser = null;
    const currentUserId = null;
    const [isEditMode, setIsEditMode] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [messageData, setMessageData] = useState<MessageData | null>(null);
    const [imageData, setImageData] = useState<ImageData | null>(null);

    const scrollArea = useRef<HTMLOListElement>(null);
    const bottomSpan = useRef<HTMLSpanElement>(null);

    //const [showDialog, setShowDialog] = useState(false);
    // const isAtBottom = useIntersection(scrollArea, bottomSpan, {
    //     rootMargin: '0px 0px 300px',
    //     threshold: 1.0
    // });
    const messagesLength = 5;
    // const [messagesSnapshot, messagesLength] = await Promise.all([
    //     getDocs(getMessagesQuery(20)),
    //     getMessagesSize()
    //   ]);
    const openModal = (data: ImageData) => (): void => {
        setIsModalOpen(true);
        setImageData(data);
    };
    const goToEditMode = (docId: string, docText: string) => (): void => {
        //setMessageData({ docId, docText });
        setIsEditMode(true);
    };
    const exitEditMode = (): void => {
        setIsEditMode(false);
        setTimeout(() => setMessageData(null), 500);
    };

    const scrollToBottom = (input?: boolean, delay?: number): void => {
        if (messageData || (input)) return;//&& isAtBottom
        setTimeout(
            () => bottomSpan.current?.scrollIntoView({ behavior: 'smooth' }),
            delay ?? 100
        );
    };
    const closeModal = (): void => setIsModalOpen(false);
    // end 
    const dialog: JSX.Element | null = showDialog
        ? (
            <dialog ref={dialogRef} >
                <div className="container-chat">
                    <div id="sidebar_secondary" className="tabbed_sidebar ng-scope chat_sidebar">
                        <div className="popup-head">
                            <div className="popup-head-left pull-left row">
                                <div className=''>
                                    {title}
                                </div>
                                <div>
                                    <i className="fa fa-window-minimize" ></i>
                                    <button className='button-close' onClick={closeDialog}>X</button>
                                    <button className='button-close' onClick={closeDialog}>-</button>
                                </div>
                            </div>
                        </div>
                        <ChatRoom
                            scrollArea={scrollArea}
                            bottomSpan={bottomSpan}
                            //isAtBottom={isAtBottom}
                            //messagesProp={messagesProp}
                            currentUserId={currentUserId}
                            messagesLength={messagesLength}
                            openModal={openModal}
                            goToEditMode={goToEditMode}
                            exitEditMode={exitEditMode}
                            scrollToBottom={scrollToBottom}
                        />
                        <div className="chat_submit_box">
                            <div className="uk-input-group">
                                <div className="gurdeep-chat-box">
                                    {/* <input type="text" placeholder="Type a message" id="submit_message" name="submit_message" className="md-input"> */}

                                    {/* <AnimatePresence>
                                        {isModalOpen && imageData && (
                                            <ImageModal imageData={imageData} closeModal={closeModal} />
                                        )}
                                    </AnimatePresence> */}
                                    {/* <TextareaAutosize
                                        className=''
                                        placeholder={
                                            currentUserId ? 'Send a message' : 'Sign in to send a message'
                                        }
                                        maxRows={5}
                                    //onChange={handleChange}
                                    //onKeyDown={!isMobile ? handleSubmit : undefined}
                                    //onPaste={handleImageUpload}
                                    //value={}
                                    //disabled={!currentUserId}
                                    //ref={inputElement}
                                    /> */}
                                    {/* <Button
                                        ariaLabel='Send message'
                                        className=''
                                        Icon={RiSendPlane2Line}
                                        //disabled={!currentUserId || (!isUploadingImages && isDisabled)}
                                        onClick={handleSendMessage}
                                    /> */}
                                    <MainForm
                                        isEditMode={isEditMode}
                                        messageData={messageData}
                                        currentUserId={currentUserId}
                                        openModal={openModal}
                                        exitEditMode={exitEditMode}
                                        scrollToBottom={scrollToBottom}
                                        handleSendMessage={handleSendMessage}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                </div >
            </dialog >
        ) : null


    return dialog
}