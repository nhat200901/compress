import { useState } from "react"
import { User } from "@/model/user";
import { loginApi } from "@/services/auth";
import Cookies from "js-cookie";
import http from "@/services/http";
import authUtil from "@/helpers/authUtil";
import { cookieKey } from "@/constants/authCommon";

export const useAuth = () => {
  const [isLogged, setIsLogged] = useState(!!authUtil.getAccessToken());

  const login = async (username: string, password: string) => {
    const user = await loginApi(username, password);
    debugger;
    if (user) {
      setIsLogged(true)
      http.setToken(user.accessToken)
      Cookies.set(cookieKey, JSON.stringify(user));
    } else {
      setIsLogged(false)
    }
    return user as User;
  };

  const logout = () => {
    setIsLogged(false)
    Cookies.remove(cookieKey);
  };

  return { login, logout, isLogged };
};
