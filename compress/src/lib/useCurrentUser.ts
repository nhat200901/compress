import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { User } from "@/model/user";
import { cookieKey } from "@/constants/authCommon";

export const useCurrentUser = () => {
  const [currentUser, setCurrentUser] = useState<User | null>(null);

  useEffect(() => {
    const currentUser = Cookies.get(cookieKey);
    if (currentUser) {
      setCurrentUser(JSON.parse(currentUser));
    }
  }, []);

  return { currentUser };
};
