import { NextResponse } from "next/server";
import { NextRequest } from "next/server";
import { authRoutes, publicRoutes } from "./router/routes";
import { cookieKey, loginPage } from "./constants/authCommon";

export function middleware(request: NextRequest) {
  const currentUser = request.cookies.get(cookieKey)?.value;
  const now = Date.now()
  if (
    ![...publicRoutes, ...authRoutes].includes(request.nextUrl.pathname) &&
    (!currentUser || now > JSON.parse(currentUser).expiredAt)
  ) {

    request.cookies.delete(cookieKey);
    const response = NextResponse.redirect(new URL(loginPage, request.url));
    response.cookies.delete(cookieKey);
    return response;
  }

  if (authRoutes.includes(request.nextUrl.pathname) && currentUser) {
    return NextResponse.redirect(new URL("/", request.url));
  }

  return NextResponse.next()
}

export const config = {
  matcher: ['/((?!api|_next/static|_next/image|favicon.ico).*)']
}
