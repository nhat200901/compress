//api for chat
export const CHATURI = {
    CreateSendMessageURI: '/api/v1/chat/CreateMessage',
    DeleteMessageURI: '/api/v1/chat/DeleteMessage',

    // for conversation
    GetConversationURI: '/api/v1/conversation/GetConversation/',
    GetListConversation: '/api/v1/conversation/GetListConversation/',
    GetListMessages: '/api/v1/conversation/GetListMessages/',
    GetListParticipants: '/api/v1/conversation/GetListParticipants/'
}
