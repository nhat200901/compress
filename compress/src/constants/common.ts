export const APP_SETTINGS = {
  NAME: 'common.titlePage',
};

export const MENU = [
  {
    name: "Dashboard",
    url: '/dashboard'
  },
  {
    name: "Order",
    url: '/order'
  },
  {
    name: "Client",
    url: '/client'
  },
  {
    name: "chat",
    url: '/chat'
  }
]
