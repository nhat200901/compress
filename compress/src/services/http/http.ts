import authUtil from '@/helpers/authUtil';
import axios, { AxiosInstance } from 'axios'

export class Http {
  protected readonly instance: AxiosInstance;
  constructor(baseUrl = "") {
    this.instance = axios.create({
      baseURL: baseUrl,
      timeout: 36000,
      timeoutErrorMessage: "Time out!",
    })

    const token = authUtil.getAccessToken();
    if (token) {
      this.setToken(token)
    }
  }

  setToken = (token: string) => {
    this.instance.defaults.headers.common['Authorization'] = `Bearer ${token}`
  }

  getInstance() {
    return {
      instance: this.instance,
      setToken: this.setToken
    };
  }

}