import { Http } from "./http";

const http = new Http(process.env.API_HOST); 
export default http.getInstance()
