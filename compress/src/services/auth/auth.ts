import http from "../http"

export const loginApiUrl = '/api/v1/account/login'

export const loginApi = async (username: string, password: string) => {
  return http.instance
    .post(loginApiUrl, {
      username: username,
      password: password
    })
    .then((res) => {
      debugger;
      const data = res.data.data

      return {
        username: data.username,
        accessToken: data.access_token,
        expiredAt: Date.now() + data.expires_in * 60 * 60,
        userId: data.userId,
        role: data.role,
      };
    });
}
