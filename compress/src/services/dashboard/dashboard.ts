import http from "../http"

export const transactionApiUrl = '/api/v1/transaction/dashboard-transaction'

export const getTransactions = async () => {
    debugger;
    const response = await http.instance.get(transactionApiUrl)
    const data = await response.data.data
    return data
}
