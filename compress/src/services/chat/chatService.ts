
import http from "../http"
import { MessageViewModel } from "@/model/chat/MessageViewModel";
import { CHATURI } from "@/constants/chatServiceUri";

export const chatMessage = async (mesage: object) => {
    return http.instance
        .post(CHATURI.CreateSendMessageURI, mesage)
        .then((res) => {
            debugger;
            const data = res.data.data

            return {
                username: data.username,
                accessToken: data.access_token,
                expiredAt: Date.now() + data.expires_in * 60 * 60,
                userId: data.userId,
                role: data.role,
            };
        });
}

export const getConversation = async (conversationId: string) => {
    return http.instance
        .get(CHATURI.GetConversationURI + conversationId)
        .then((res) => {
            debugger;
            const data = res.data.data

            return {
                username: data.username,
                accessToken: data.access_token,
                expiredAt: Date.now() + data.expires_in * 60 * 60,
                userId: data.userId,
                role: data.role,
            };
        });
}
export const getMesageConversation = async (conversationId: string) => {
    return http.instance
        .get(CHATURI.GetListMessages + conversationId)
        .then((res) => {
            debugger;
            const data = res.data.data

            return {
                username: data.username,
                accessToken: data.access_token,
                expiredAt: Date.now() + data.expires_in * 60 * 60,
                userId: data.userId,
                role: data.role,
            };
        });
}