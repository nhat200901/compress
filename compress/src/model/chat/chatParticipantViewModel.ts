import { ChatParticipantTypeEnum } from "@/enums/chatParticipantTypeEnum";
export type ChatParticipantViewModel = {
    participantType: ChatParticipantTypeEnum;
    id: string;
    status: string;
    avatar: string;
    displayName: string;
    userId: string;
};