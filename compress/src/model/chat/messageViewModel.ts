export type MessageViewModel = {
    id: string;
    messageType: string;
    senderUserName: number;
    senderUserId: string;
    senderUserConnextionId: string;
    recieverUserName: string;
    recieverUserId: string;
    recieverUserConnextionId: string;
    message: string;
    dateSent: string;
    fileId: string;
    downloadUrl: string;
    fileSizeInBytes: string;
    isFile: string;
    chatStatus: number;
    conversationId: string;
};