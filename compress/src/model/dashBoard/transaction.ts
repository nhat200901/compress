export interface Transaction {
    orderId: number
    accountType: string
    type: string
    actionExchange: string
    amount: number
    orderDate: Date
    remarks: string
}