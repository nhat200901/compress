export type User = {
  username: string;
  userId: string;
  expiredAt: number;
  accessToken: string;
  avatar?: string;
  id: string;
  customerServiceId: string;
  role: string;
};
