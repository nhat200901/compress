import { cookieKey } from "@/constants/authCommon";
import Cookies from "js-cookie";

const authUtil = {
  getAccessToken() {
    const currentUser = Cookies.get(cookieKey) ?? "{}";
    return JSON.parse(currentUser)?.accessToken || ""
  }
};
export default authUtil

